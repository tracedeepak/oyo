$(document).ready(function(){
    $('#btnTravelDetails').click(function(){
        var data = $('#frmTravelDetails').serialize();
        formData(data);
    });

    $('.closeBtn').click(function(){
        $('.termsBanner').hide('slow');
    });
    
});

jQuery.fn.extend({
	inputChangeAction: function(){
        var targetImgDiv = $(this).closest('.formUploadBox').find('.imgBox .uploadImgBox figure img');
        if(typeof targetImgDiv != 'undefined'){
            if ($(this)[0].files && $(this)[0].files[0] && $.inArray($(this)[0].files[0].type,imageExtentions)>=0) {
                var reader = new FileReader();
                reader.onload = function(e) {
                  targetImgDiv.attr('src', e.target.result);
                }
                reader.readAsDataURL($(this)[0].files[0]);
            }else{
                targetImgDiv.attr('src',$(this).data('src'));
            }
        }else{
            $(this).validateData() ? $(this).inputErrorHide(): $(this).inputErrorShow();
        }
    },
    clickAction: function () {
        // code for Mylead
        if($(this).parent("div").attr("id") == "modalProfileCancel"){
            // $(this).parents("div.modal").modal('toggle');        
        }
        else if ($(this).attr("id") == "btnTravelDetails") {
            snd.pause();
            snd.currentTime=0;
            var data = $('#formData').serialize();
            formData(data);
        }
    },
    inputKeyPressAction: function (event) {
        if ($(this).data("type") != undefined)
            var code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
        if ($(this).data("type") == "INR" || $(this).data("type") == "int" || $(this).data("type") == "mobile") {
            if (code == 37 || code == 38 || code == 39 || code == 40) {
                return false;
            }
            if ((code >= 48 && code <= 57) || code == 8 || code == 9 || code == 37 || code == 38 || code == 39 || code == 40 || code == 46) {
                return true;
            } else {
                return false;
            }
        } else if ($(this).data("type") == "alphabetic") {
            if (code >= 48 && code <= 57) {
                event.preventDefault();
            }
        
        }
    },
    inputKeyBlurAction: function () {
        $(this).validateData() ? $(this).inputErrorHide(): $(this).inputErrorShow();
    },
    inputUpAction: function () {
        var whitespaceRegex = /\s\s+/;
        if(whitespaceRegex.test($(this).val())){
            $(this).val($(this).val().replace(/\s\s+/,' '));
        }
        
    },
    validateData: function(){
        if($(this).attr('type') != 'file'){
            $(this).val($.trim($(this).val()));
        }
        var isValidated = true;
        var isNull = false;
        var re = "";
        var name = $(this).attr("name");        
        var isChktst = "pregMatch";
        var fieldBlank = false;
        if($(this).val() == null || $(this).val() == 0 || $(this).val() == 0){
            fieldBlank = true;
        }
        switch(name){
            case "checkin":
                re= /(\d+)(-|\/)(\d+)(?:-|\/)(?:(\d+)\s+(\d+):(\d+)(?::(\d+))?(?:\.(\d+))?)?/
                break;
            case "checkout":
                re= /(\d+)(-|\/)(\d+)(?:-|\/)(?:(\d+)\s+(\d+):(\d+)(?::(\d+))?(?:\.(\d+))?)?/
                break;
            case "email":
                re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                break;
            case 'name':
                re = /^[a-z ]{3,}$/i;
                break;
            case 'selTravellers':
                isChktst = "numeric"
                break;
            case 'phone':
                isChktst = "numeric";
                break;               
        }

        if($(this).closest('form').find('input[name=validationType]').val()=='full'){
            isNull =false;
        }

        if(isNull && fieldBlank ){
            isChktst = false;
            isValidated = true;
        }else{
            if(isChktst == "required"){
                isChktst = false;
                isValidated = false;
                if(!fieldBlank){
                    isValidated = true;
                }
            } else if(isChktst == "pregMatch" && re != ""){
                    isValidated = re.test($(this).val()) ? true: false;
            } else if(isChktst == "numeric"){
                isChktst = false;
                isValidated = false;
                if(!fieldBlank && $.isNumeric($(this).val())){
                    isValidated = true;
                }
            }
        }
        // alert(isValidated);
        return isValidated;
    },
    inputErrorShow:function(error = '', levelUp = 0){
        var element = $(this);
        
        if(!error){
            var errroMsg = ErrorMap($(this).attr("name"));
        }else{
            var errroMsg = error;
        }
        element.siblings('span.custom-error').attr('title',errroMsg).text(errroMsg).show("slow");
        element.parents().siblings('span.custom-error').attr('title',errroMsg).text(errroMsg).show("slow");
        element.parents().parents().siblings('span.custom-error').attr('title',errroMsg).text(errroMsg).show("slow");
    },
    inputErrorHide: function(){
        $(this).siblings('span.custom-error').text('').hide("slow");
         $(this).parents().siblings('span.custom-error').text('').hide("slow");
          $(this).parents().parents().siblings('span.custom-error').text('').hide("slow");
    }
});

formValidate= function(formId){
    var frmValidated = true;
    $.each($('input,select,textarea,tel', formId), function () {
        ele_validated = "";
        if ($(this).attr("type") != "hidden"){
            if ( $(this).validateData()) {
                    $(this).inputErrorHide();
            } else {
                    $(this).inputErrorShow();
                frmValidated = false;
            }
        }
    });
    return frmValidated;
}

clearForm=function(formId){
    $.each($('input,select,textarea,tel', formId), function () {
        if(!$(this).is("select")) {
            $(this).prop('selectedIndex', 0);
        }else{
            $(this).val('');
        }
        
    });
}

ErrorMap = function(name){
    var error = "";
    switch(name){
        case "email":
            error = 'Invalid Email';
            break; 
        case "name":
            error = 'Please Enter Name';
            break;    
        case "checkin":
            error = 'Invalid Checkin Date';
            break;      
        case "checkout":
            error = 'Invalid Checkout Date';
            break;
        case "phone":
            error = 'Please Enter Mobile Number';
            break;
        case "selTravellers":
            error = 'Please Choose # of Travelers';
            break;
        default:
            error = "Please Enter Valid Data";
    }
    return error;
}

function formData(data){
    var formId = $('#frmTravelDetails');
        // var url = APP_BASE_PATH + 'builder/edit-builder-profile';
        if(formValidate(formId)){
        $.ajax({
            method: "POST",
            url: 'send_quote.php',
            data: data,
            dataType    : 'json',
            headers : {
                'X-CSRF-TOKEN': $('input[name="_token"]').val(),
            },
            beforeSend: function() {
                $('#btnTravelDetails').text('please wait...');
                //$('#popupLoaderBox').show();
            },
            success: function( response ) {
                $('#btnTravelDetails').text('Get Our Best Offers...!!!');
                if(response.error == 1){
                    $('span.error').text(response.message).show();
                }else{
                    $('#travelDetailBox').modal('hide');
                    $('#thanku').modal('show');
                    clearForm($('#frmTravelDetails'));
                }
            },
            error: function(response ,httpObj, textStatus){
            }
        });
    }
}


